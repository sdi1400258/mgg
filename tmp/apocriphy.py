# These functions were discarded because reachability analysis
# was not considered important

# Determines if a node is reachable in the graph, returns a boolean
def is_node_reachable(lang: dict, graph: List[AtkGraphNode], node: AtkGraphNode) -> bool:
    """
    Determines if a node is reachable in the graph.

    This function checks if the given node is reachable from any of the attached nodes in the graph.
    The reachability of a node depends on its type ('and' or 'or') and the reachability of its parent nodes.

    Args:
        lang (dict): A dictionary representing the whole grammar of the
                     a mal language.
        graph: An mgg attack graph generated with 
                                    generate_graph() function or loaded
                                    from another file.
        node : The node to check for reachability.

    Returns:
        bool: True if the node is reachable, False otherwise.
    """
    # Check the node type and call the corresponding function
    if node.type == "and":
        return is_and_node_reachable(lang,graph,node)
    elif node.type == "or":
        return is_or_node_reachable(graph,node)

# Checks if an 'or' node is reachable, return a boolean
def is_or_node_reachable(graph: List[AtkGraphNode], node: AtkGraphNode) -> bool:
     """
    Checks if an 'or' node is reachable in the graph.

    This function checks if any of the parent nodes of the given 'or' node are marked as reachable.

    Args:
        graph : An mgg attack graph generated with 
                                    generate_graph() function or loaded
                                    from another file.
        node: The 'or' node to check for reachability.

    Returns:
        bool: True if the node is reachable, False otherwise.
    """
    # Get the parents of the node
    parents = get_node_parents(graph, node.id)
    # Check if any parent node is marked as reachable
    return any([get_node_by_id(graph, parent).is_reachable for parent in parents])

# Checks if an 'and' node is reachable, and returns a boolean
def is_and_node_reachable(lang: dict, graph: List[AtkGraphNode], node: AtkGraphNode) -> bool:
    """
    Checks if an 'and' node is reachable in the graph.

    This function checks if all required internal and external parents of the given 'and' node are satisfied.

    Args:
        lang : A dictionary representing the whole grammar of the
                     a mal language.
        graph : An mgg attack graph generated with 
                                    generate_graph() function or loaded
                                    from another file.
        node : The 'and' node to check for reachability.

    Returns:
        bool: True if the node is reachable, False otherwise.
    """
    # Make sure the node is of type 'and'
    if not node.type == "and":
        return True
    # Helper function to check if an internal parent is satisfied
    def is_internal_parent_satisfied(node, atkgraph_node_parents, parent_name):
        for node_parent in atkgraph_node_parents:
            cls, objid, atkname = node_parent.split(":")
            if objid == node.objid and atkname == parent_name:
                return True
        return False
    # Get the parents of the node
    atkgraph_node_parents = get_node_parents(graph, node.id)
    
    # Check if all parent are traversable
    for parent in atkgraph_node_parents:
        parent_node = get_node_by_id(graph, parent)
        if not parent_node.is_traversable:
            return False

    # Get the required internal and external parents
    req_parents = l.get_parent_steps(lang, node.objclass, node.atkname)

    req_internal_parents = [parent for parent in req_parents if parent[0] == "-"]
    req_external_parents = [parent for parent in req_parents if parent[0] != "-"]

    # Check if all required internal parents are satisfied
    for parent in req_internal_parents:
        parent_name = parent[1]
        if not is_internal_parent_satisfied(node, atkgraph_node_parents, parent_name):
            return False

    # Check if all required external parents are satisfied
    for parent in req_external_parents:
        cls = parent[0]
        atkname = parent[1]
        objects_to_check = get_nodes_by_class(graph, cls)
        parents_to_satisfy = [obj for obj in objects_to_check if obj.endswith(":"+atkname)]
        for parent in parents_to_satisfy:
            if parent not in atkgraph_node_parents:
                return False
    return True

# Attaches an attacker to specific nodes and computes reachability
def attach_attacker_and_compute(lang: dict, graph: List[AtkGraphNode], node_ids: List[str]) -> List[AtkGraphNode]:
    """
    Attaches an attacker to specific nodes and computes reachability.

    This function attaches an attacker to specific nodes in the graph and updates the reachability status of all other nodes.

    Args:
        lang : A dictionary representing the whole grammar of the
                     a mal language.
        graph : An mgg attack graph generated with 
                                    generate_graph() function or loaded
                                    from another file.
        node_ids : A list of ids of nodes to attach the attacker to.

    Returns:
        List[AtkGraphNode]: An updated graph where all nodes have been marked with their reachability status.
    """
    # Helper function to update reachability status
    def set_reachability(lang, graph):
        newgraph = []
        for node in graph:
            newnode = node
            # Check if the node is not in the list of attached nodes
            if newnode.id not in node_ids:
                # Update the reachability status of the node
                newnode.is_reachable = is_node_reachable(lang, graph, node)
            newgraph.append(newnode)
        return newgraph

    # Attach the attacker to the specidied nodes
    for attached_node in node_ids:
        graph = attach_attacker_to_node(graph, attached_node)
    # Compute reachability for all nodes in the graph 
    graph = set_reachability(lang, graph)
    return graph

# Prunes untraversable nodes from the graph
def prune_untraversable(graph: List[AtkGraphNode]) -> List[AtkGraphNode]:
    """
    Prunes untraversable nodes from the graph.

    This function removes all untraversable nodes from the graph and updates the links of the remaining nodes accordingly.

    Args:
        graph : An mgg attack graph generated with 
                                    generate_graph() function or loaded
                                    from another file.

    Returns:
        List[AtkGraphNode]: An updated graph where all untraversable nodes have been removed.
    """
    newgraph = []
    nodes_to_remove = []
    for node in graph:
        if node.is_traversable:
            newgraph.append(node)
        else:
            nodes_to_remove.append(node.id)
    # Remove links to untraversable nodes
    for node in graph:
        for link in nodes_to_remove:
            if link in node.links: node.links.remove(link)
    return newgraph

# Prunes unreachable nodes from the graph
def prune_unreachable(graph: List[AtkGraphNode]) -> List[AtkGraphNode]:
    """
    Prunes unreachable nodes from the graph.

    This function removes all unreachable nodes from the graph and updates the links of the remaining nodes accordingly.

    Args:
        graph : An mgg attack graph generated with 
                                    generate_graph() function or loaded
                                    from another file.
    Returns:
        List[AtkGraphNode]: An updated graph where all unreachable nodes have been removed.
    """
    newgraph = []
    nodes_to_remove = []
    for node in graph:
        if node.is_reachable:
            newgraph.append(node)
        else:
            nodes_to_remove.append(node.id)
    # Remove links to unreachable nodes
    for node in graph:
        for link in nodes_to_remove:
            if link in node.links: node.links.remove(link)
    return newgraph


# Retrieve nodes in the graph, based on their class
def get_nodes_by_class(graph, cls):
    """
    Retrieves nodes in the graph based on their class.

    This function searches the graph for nodes that belong to the specified class and returns a list of their ids.

    Args:
        graph: An mgg attack graph generated with 
                                    generate_graph() function or loaded
                                    from another file.
        cls (str): The class to search for.

    Returns:
        List[str]: A list of node ids that belong to the specified class.
    """
    nodes = select(where(cls=cls), graph)
    objs = set()
    for node in nodes:
        objs.add(node.id)
    return list(objs)


# Attaches an attacker to a specific node in the graph
def attach_attacker_to_node(graph: List[AtkGraphNode], node_id: str) -> List[AtkGraphNode]:
        """
        Attaches an attacker to a specific node in the graph.

        This function marks the specified node as reachable and updates the traversability status of all nodes that can be reached from it.

        Args:
            graph: An mgg attack graph generated with 
                                    generate_graph() function or loaded
                                    from another file.
            node_id (str): The id of the node to attach the attacker to.

        Returns:
            List[AtkGraphNode]: An updated graph where the specified node has been 
                                marked as reachable and all nodes that can be traversed 
                                from it have been marked as traversable.
        """
        node = get_node_by_id(graph, node_id)
        node.is_reachable = True
        visited_nodes = []

        # Recursive function to traverse the graph and mark nodes as traversable
        def traverse(node):
            if node.id in visited_nodes:
                return
            node.is_traversable = True
            visited_nodes.append(node.id)
            for link in node.links:
                linknode = get_node_by_id(graph, link)
                traverse(linknode)

        traverse(node)
        return graph

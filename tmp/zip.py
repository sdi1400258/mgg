import os
import json
import mgg.securicad as s
from securicad import enterprise
import mgg.model as m


config_file = "/home/gnc/coa_gn.ini"
client = s.scad_connect_from_config(config_file)

project = client.projects.get_project_by_name("test")
models = enterprise.models.Models(client)
model_info = models.get_model_by_name(project, "PE test4")
model = model_info.get_model()


model = s.get_model(client, "test", "PE test4")
import json

s.save_model(model,"petest4.json")
with open("petest4.json", 'w') as f:
    json.dump(model.model, f, indent=4)


datapath = 'data-models'
if not os.path.exists(datapath):
    os.makedirs(datapath)
model_path = "data-models/temp.sCAD"
scad_dump = model_info.get_scad()
print("model downloaded")
f1 = open(model_path, "wb")
f1.write(scad_dump)
f1.close()

# unzip the model
model_dir_path = model_path[:model_path.rindex('/')]
model_file_name = model_path[model_path.rindex('/') + 1:model_path.rindex('.')]
unzip_dir = "scad_dir"
unzip_dir_path = "{}/{}".format(model_dir_path, unzip_dir)
import zipfile
with zipfile.ZipFile(model_path, 'r') as zip_ref:
    zip_ref.extractall(unzip_dir_path)
eom_path = "{}/{}.eom".format(unzip_dir_path, model_info.name)
print("model unzipped in  -- ", unzip_dir_path)

# delete the downloaded model file
os.remove(model_path)
print("downloaded model deleted")

import xml.etree.ElementTree as ET
# xml parsing


with open(eom_path, 'rt') as f:
    tree = ET.parse(f)
    root = tree.getroot()


objects = []
assocs = []


for object in root.iter("objects"):
    model_dict = {}
    model_dict[object.attrib['id']] = { 
            "name": object.attrib['name'],
            "metaconcept": object.attrib['metaConcept'],
            "eid": object.attrib['exportedId'],
            "attributes": json.loads(object.attrib['attributesJsonString'])}
    objects.append(model_dict)
    #print(model_dict_list)


for object in root.iter("associations"):
    assocs.append({"id1": object.attrib['sourceObject'],
                   "id2": object.attrib['targetObject'],
                   "type1": object.attrib['sourceProperty'],
                   "type2": object.attrib['targetProperty']})



m.load_model_from_json("tests/assets/PE_test4.json")

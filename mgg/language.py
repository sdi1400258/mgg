# -*- encoding: utf-8 -*-
# mgg v0.1.0
# Generate MAL-based attack graphs
# Copyright 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.

"""
mgg language Module

:Copyright: 2021, Giuseppe Nebbione.
:License: GPLv3 (see /LICENSE).
"""
import json
import mgg.model as m


def load_language_specification(lang_spec: str) -> dict:
    """
    Read a MAL language JSON specification file

    Arguments:
    file_spec       - a language specification file that can be for example
                      provided by malc (https://github.com/mal-lang/malc)

    Return:
    A dictionary representing the whole grammar of the MAL language
    """
    with open(lang_spec, 'r', encoding='utf-8') as spec:
        data = spec.read()
    return json.loads(data)


def save_language_specification(lang_spec: dict, filename: str) -> dict:
    """
    Saves a MAL language specification dictionary to a JSON file

    Arguments:
    file_spec       - a language specification file that can be for example
                      provided by malc (https://github.com/mal-lang/malc)
    filename        - the JSON filename that will be saved
    """
    lang = {}
    lang['name'] = lang_spec['defines']['id']
    lang['assets'] = get_attack_tree(lang_spec)
    lang['associations'] = []
    for assoc in lang_spec["associations"]:
        del assoc['meta']
        lang['associations'].append(assoc)


    with open(filename, 'w', encoding='utf-8') as file:
        json.dump(lang, file, indent=4)


def get_classes(lang_spec: dict) -> list:
    """
    Get all available object classes in the language specification

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      mal language as provided by `read_language_spec(...)`

    Return:
    A list of strings representing the available classes in the language
    """
    return [objclass["name"] for objclass in lang_spec['assets']]


def old_get_attacks_for_class(lang_spec: dict, object_class: str) -> dict:
    """
    Get all Attack Steps for a specific Class

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      mal language as provided by `read_language_spec(...)`
    object_class    - a string representing the class for which we want to list
                      the possible attack steps

    Return:
    A dictionary representing the set of possible attacks for the specified
    class. Each key in the dictionary is an attack name and is associated
    to a dictionary containing other characteristics of the attack such as
    type of attack, TTC distribution, child attack steps and other information
    """
    attacks = {}

    def _get_attacks(object_class):
        nonlocal attacks
        for asset in lang_spec["assets"]:
            if asset["name"] == object_class:
                attack_steps = asset["attackSteps"]
                for atk in attack_steps:
                    if atk["type"] != "defense":
                        if not atk["name"] in attacks:
                            attacks[atk["name"]] = atk
                if asset["superAsset"]:
                    _get_attacks(asset["superAsset"])

    _get_attacks(object_class)
    return attacks

def get_attacks_for_class(lang_spec: dict, object_class: str) -> dict:
    """
    Get all Attack Steps for a specific Class

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      mal language as provided by `read_language_spec(...)`
    object_class    - a string representing the class for which we want to list
                      the possible attack steps

    Return:
    A dictionary representing the set of possible attacks for the specified
    class. Each key in the dictionary is an attack name and is associated
    to a dictionary containing other characteristics of the attack such as
    type of attack, TTC distribution, child attack steps and other information
    """
    attacks = {}

    def _get_attacks(object_class):
        nonlocal attacks
        for asset in lang_spec["assets"]:
            if asset["name"] == object_class:
                attack_steps = asset["attackSteps"]
                for atk in attack_steps:
                    if not atk["name"] in attacks:
                        attacks[atk["name"]] = atk
                if asset["superAsset"]:
                    _get_attacks(asset["superAsset"])

    _get_attacks(object_class)
    return attacks


def get_defenses_for_class(lang_spec: dict, object_class: str) -> dict:
    """
    Get all Defenses available for a specific Class

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      a MAL language as provided by `read_language_spec(...)`
    object_class    - a string representing the class for which we want to list
                      the possible defenses

    Return:
    A dictionary representing the set of possible defenses for the specified
    class. Each key in the dictionary is a defense name and is associated
    to a dictionary containing other characteristics of the defense
    """
    defenses = {}

    def _get_defenses(object_class):
        nonlocal defenses
        for asset in lang_spec["assets"]:
            if asset["name"] == object_class:
                attack_steps = asset["attackSteps"]
                for defense in attack_steps:
                    if defense["type"] == "defense":
                        if not defense["name"] in defenses:
                            defenses[defense["name"]] = defense
                if asset["superAsset"]:
                    _get_defenses(asset["superAsset"])

    _get_defenses(object_class)
    return defenses


def get_super_classes(lang_spec: dict, object_class: str) -> list:
    """
    Get all the parent classes related to a class

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      a MAL language as provided by `read_language_spec(...)`
    object_class    - a string representing the class for which we want to
                      retrieve the parent classes

    Return:
    A list representing the parent classes of the provided class.
    The list is ordered from left to right, e.g.,
    ['MotherClass', 'GrandMotherClass', 'GrandGrandMotherClass']
    """
    super_classes = []

    def _get_super_classes(object_class):
        nonlocal super_classes
        for asset in lang_spec["assets"]:
            if asset["name"] == object_class and asset["superAsset"]:
                super_classes.append(asset["superAsset"])
                _get_super_classes(asset["superAsset"])

    _get_super_classes(object_class)
    return super_classes


def get_associations_for_class(lang_spec: dict, object_class: str) -> list:
    """
    Get all the possible associations related to a class

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      a MAL language as provided by `read_language_spec(...)`
    object_class    - a string representing the class for which we want to
                      retrieve the possible associations

    Return:
    A list of dictionaries representing the possible associations that are
    available for a specific class
    """
    assocs = []

    def _get_assocs(object_class):
        nonlocal assocs
        for asset in lang_spec["associations"]:
            if object_class in [asset["leftAsset"], asset["rightAsset"]]:
                assocs.append({
                    "name": asset["name"],
                    "leftClass": asset["leftAsset"],
                    "leftField": asset["leftField"],
                    "rightClass": asset["rightAsset"],
                    "rightField": asset["rightField"]})

    classes = get_super_classes(lang_spec, object_class)
    classes.insert(0, object_class)

    for objclass in classes:
        _get_assocs(objclass)

    # Remove Duplicate Associations
    assocs = [dict(t) for t in {tuple(d.items()) for d in assocs}]
    return assocs

def get_front_associations_for_class(lang_spec: dict, object_class: str) -> list:
    """
    Get all the possible associations related to a class but only information
    regarding the same end of the class

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      a MAL language as provided by `read_language_spec(...)`
    object_class    - a string representing the class for which we want to
                      retrieve the possible associations

    Return:
    A list of dictionaries representing the possible associations that are
    available for a specific class
    """
    assocs = []

    def _get_assocs(object_class):
        nonlocal assocs
        for asset in lang_spec["associations"]:
            if object_class == asset["leftAsset"]:
                assocs.append({
                    "name": asset["name"],
                    "otherClass": asset["leftAsset"],
                    "otherField": asset["leftField"]})
            elif object_class == asset["rightAsset"]:
                assocs.append({
                    "name": asset["name"],
                    "otherClass": asset["rightAsset"],
                    "otherField": asset["rightField"]})

    classes = get_super_classes(lang_spec, object_class)
    classes.insert(0, object_class)

    for objclass in classes:
        _get_assocs(objclass)

    # Remove Duplicate Associations
    assocs = [dict(t) for t in {tuple(d.items()) for d in assocs}]
    return assocs

def get_opposite_associations_for_class(lang_spec: dict, object_class: str) -> list:
    """
    Get all the possible associations related to a class but only information
    regarding the other end of the association

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      a MAL language as provided by `read_language_spec(...)`
    object_class    - a string representing the class for which we want to
                      retrieve the possible associations

    Return:
    A list of dictionaries representing the possible associations that are
    available for a specific class
    """
    assocs = []

    def _get_assocs(object_class):
        nonlocal assocs
        for asset in lang_spec["associations"]:
            if object_class == asset["leftAsset"]:
                assocs.append({
                    "name": asset["name"],
                    "otherClass": asset["rightAsset"],
                    "otherField": asset["rightField"]})
            elif object_class == asset["rightAsset"]:
                assocs.append({
                    "name": asset["name"],
                    "otherClass": asset["leftAsset"],
                    "otherField": asset["leftField"]})

    classes = get_super_classes(lang_spec, object_class)
    classes.insert(0, object_class)

    for objclass in classes:
        _get_assocs(objclass)

    # Remove Duplicate Associations
    assocs = [dict(t) for t in {tuple(d.items()) for d in assocs}]
    return assocs



# SHOULD BE OK BUT BETTER RE-CHECK/TEST FOR EDGE CASES
# This is a temporary unused function, do we actually need this?
def get_associations_by_field(lang_spec, object_class, field):
    """
    This function is temporary and should be tested, at the
    moment it is not used. Its goal would be to retrieve
    associations for a class once a field (i.e., role) is provided
    """
    assocs = get_associations_for_class(lang_spec, object_class)

    assocs_by_field = []
    for assoc in assocs:
        if field == assoc["leftField"]:
            assocs_by_field.append(assoc["leftClass"])
        elif field == assoc["rightField"]:
            assocs_by_field.append(assoc["rightClass"])
    return assocs_by_field


def build_attack_step_name(attack_step: dict) -> str:
    """
    Gets the MAL (malc) language specification of an attack step
    and returns its simple string representation.
    Example:

    Input
        {'type': 'collect',
            'lhs': {'type': 'variable',
                    'name': 'allVulnerabilities'},
            'rhs': {'type': 'attackStep',
                    'name': 'localAccessAchieved'}
        }

    Output:
        "allVulnerabilities.localAccessAchieved"



    Arguments:
    attack_step     - the MAL (malc) language specification of an attack step.
                      The list containing these definitions can be provided by:
                        all_atks = get_attacks_for_class(lang,"Application")
                        atks["localConnect"]["reaches"]["stepExpressions"]

    Return:
    A string representing the simple form of the attack step name similarly
    to what can be found in MAL syntax files (i.e., ".mal" files).
    """
    atkstep_name = ""

    def _buildatk(rlhs):
        if rlhs["type"] == "attackStep":
            return atkstep_name + rlhs["name"]

        if rlhs["type"] == "subType":
            return atkstep_name + \
                    rlhs["stepExpression"]["name"] + \
                    "[" + rlhs["subType"] + "]" + "."

        if rlhs["type"] == "transitive":
            return atkstep_name + \
                    rlhs["stepExpression"]["name"] + \
                    "*" + "."

        if rlhs["type"] == "collect":
            return atkstep_name + \
                _buildatk(rlhs["lhs"]) + _buildatk(rlhs["rhs"])

        if rlhs["type"] == "union":
            return atkstep_name + \
                _buildatk(rlhs["lhs"])[:-1] + "\/" + _buildatk(rlhs["rhs"])

        if rlhs["type"] == "intersection":
            return atkstep_name + \
                _buildatk(rlhs["lhs"])[:-1] + "/\\" + _buildatk(rlhs["rhs"])

        if rlhs["type"] == "difference":
            return atkstep_name + \
                _buildatk(rlhs["lhs"])[:-1] + "-" + _buildatk(rlhs["rhs"])

        if rlhs["type"] == "field":
            return atkstep_name + rlhs["name"] + "."

        if rlhs["type"] == "variable":
            return atkstep_name + rlhs["name"] + "()" + "."

        return atkstep_name

    return _buildatk(attack_step)


def get_required_steps(
        lang: dict,
        object_class: str,
        atk_name: str) -> list:
    """
    Provides the list of required attack steps belonging to an attack of type
    "exist" and "notExist"

    Arguments:
    lang            - the MAL (malc) language specification of an attack step.
    object_class    - a string representing the class for which we want to
                      retrieve the possible attack steps
    atk_name        - a string representing the attack for which we want to
                      retrieve the required attack steps (e.g., "localConnect")

    Return:
    A list of strings, each representing a required attack steps of the provided
    attack name
    """
    atksteps = []

    def _get_required_steps(objclass):
        nonlocal atksteps
        class_atks = get_attacks_for_class(lang, objclass)
        try:
            for atkstep in class_atks[atk_name]["requires"]["stepExpressions"]:

                depname = build_attack_step_name(atkstep)
                if depname.endswith("."):
                    depname = depname[:-1]
                atksteps.append(depname)
                if not class_atks[atk_name]["requires"]["overrides"]:
                    super_class = get_super_classes(lang,objclass)[0]
                    _get_required_steps(super_class)

        except TypeError:
            pass

    _get_required_steps(object_class)
    return atksteps

def get_child_steps(
        lang: dict,
        object_class: str,
        atk_name: str) -> list:
    """
    Provides the list of child attack steps belonging to an attack

    Arguments:
    lang            - the MAL (malc) language specification of an attack step.
    object_class    - a string representing the class for which we want to
                      retrieve the possible attack steps
    atk_name        - a string representing the attack for which we want to
                      retrieve the child attack steps (e.g., "localConnect")

    Return:
    A list of strings, each representing a child attack steps of the provided
    attack name
    """
    atksteps = []

    def _get_child_steps(objclass):
        nonlocal atksteps
        class_atks = get_attacks_for_class(lang, objclass)
        try:
            for atkstep in class_atks[atk_name]["reaches"]["stepExpressions"]:

                atksteps.append(build_attack_step_name(atkstep))
                if not class_atks[atk_name]["reaches"]["overrides"]:
                    super_class = get_super_classes(lang,objclass)[0]
                    _get_child_steps(super_class)

        except TypeError:
            pass

    _get_child_steps(object_class) 
    return atksteps



def is_internal_attack_step(attack_step: str) -> bool:
    """
    Returns True if the attack step is internal (or simple).
    Examples:
        "localConnect" returns True
        "app.localConnect" returns False
        "app.fw.localConnect" returns False
        "routerFirewall().localConnect" returns False

    Arguments:
    attack_step    - a string representing an attack step


    Return:
    A boolean that is True if the provided attack is internal (or simple)
    """
    no_dots = "." not in attack_step
    no_parenthesis = not "()" in attack_step
    return no_dots and no_parenthesis




def get_attack_tree(lang_spec: dict):
    """
    Gives the attack tree of a whole language specification
    in the form of a dictionary of dictionaries.
    The keys of this dictionary represent the available
    classes in the language. To each key (class) 
    is associated a dictionary whose keys are attacks
    and whose values are lists of child attack steps.


    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      mal language as provided by `read_language_spec(...)`


    Return:
    A dictionary representing the whole attack tree of the input language
    """
    attack_tree = {}

    for cls in get_classes(lang_spec):
        attack_tree[cls] = {}
        for atk,attribs in get_attacks_for_class(lang_spec, cls).items():
            atk_steps = get_child_steps(lang_spec, cls, atk)
            attack_tree[cls][atk] = {}
            attack_tree[cls][atk]["child_steps"] = atk_steps 
            attack_tree[cls][atk]["type"] = attribs['type'] 
            attack_tree[cls][atk]["ttc"] = attribs['ttc'] 
            if attribs['type'] in ("exist","notExist"):
                required_steps = get_required_steps(lang_spec, cls, atk)
                attack_tree[cls][atk]["required_for_existence"] = required_steps

    return attack_tree


def is_step_resolvable(
        lang: dict, origin_cls: str, target_cls: str, step: str
) -> bool:
    """
    Determines if an attack step is resolvable.
    This function returns True, if considering a specific
    language, with a specific origin class, an attack step
    is resolvable with respect to the target class.
    

    Arguments:
    lang            - a dictionary representing the whole grammar of the
                      mal language as for example provided by `read_language_spec(...)`
    origin_cls      - the origin class which would contain the attack step
    target_cls      - the target class to which the attack name is supposedly referred to
    step            - the attack step name


    Return:
    A boolean that is true when the attack name (or step) is resolvable.
    """
    complex_step = step.split(".")
    atkname = complex_step.pop()
    
    if atkname not in get_attacks_for_class(lang, target_cls):
        return False

    def _is_resolvable(lang, origin_cls, target_cls, complex_step):
        assocs = get_associations_for_class(lang, origin_cls)
        if len(complex_step) == 1:
            for assoc in assocs:
                role_field = complex_step[-1]
                if assoc['leftField'] == role_field and assoc['leftClass'] == target_cls:
                    return True
                if assoc['rightField'] == role_field and assoc['rightClass'] == target_cls:
                    return True
            return False
        else:
            for assoc in assocs:
                if assoc['leftClass'] == origin_cls:
                    role_field = complex_step[0]
                    if assoc['rightField'] == role_field:
                        return _is_resolvable(lang, assoc['rightClass'], target_cls, complex_step[1:])
                if assoc['rightClass'] == origin_cls:
                    role_field = complex_step[0]
                    if assoc['leftField'] == role_field:
                        return _is_resolvable(lang, assoc['leftClass'], target_cls, complex_step[1:])
            return False

    return _is_resolvable(lang,origin_cls,target_cls,complex_step)



def get_internal_parents(lang: dict, cls: str, atkname: str) -> list:
    """
    Gets a list of the internal parents of a specific attack step.

    Arguments:
    lang            - a dictionary representing the whole grammar of the
                      mal language as for example provided by `read_language_spec(...)`
    cls             - the class to which the attack name belongs to
    atkname         - the attack step name


    Return:
    A list of tuples representing parent attack steps belonging
    to the same class.  Each tuple is in the form (origin_class, parent_attack_step),
    although it is redundant to have the origin_class
    attached to each tuple, this makes it more compatible and conform with
    `get_external_parents`.
    """
    attack_tree = get_attack_tree(lang)
    parents = []
    for parent,attribs in attack_tree[cls].items():
        if atkname in attribs["child_steps"]:
            parents.append(("-",parent))
    return parents

def get_external_parents(lang: dict, cls: str, atkname: str) -> list:
    """
    Gets a list of the external parents of a specific attack step.
    By external it is meant all the attack steps belonging to other
    classes which have as child attack steps an attack step belonging
    to the target class.

    Arguments:
    lang            - a dictionary representing the whole grammar of the
                      mal language as for example provided by `read_language_spec(...)`
    cls             - the class to which the attack name belongs to
    atkname         - the attack step name


    Return:
    A list of parent attack steps belonging to other classes
    """
    parents = []
    attack_tree = get_attack_tree(lang)
    for origin_cls,atk in attack_tree.items():
        if origin_cls in get_super_classes(lang,cls) + [cls]:
            continue
        for parent_atk,atkattribs in atk.items():
            for step in atkattribs["child_steps"]:
                if "()" in step:
                    # TODO
                    continue
                elif "." in step and step.split(".")[-1] == atkname:
                    if is_step_resolvable(lang, origin_cls, cls, step):
                        parents.append((origin_cls,parent_atk))
    return list(dict.fromkeys(parents))



def get_parent_steps(lang: dict, cls: str, atkname: str) -> list:
    """
    Get the list of all the parent steps referred to a specific attack.

    Arguments:
    lang            - a dictionary representing the whole grammar of the
                      mal language as for example provided by `read_language_spec(...)`
    cls             - the class to which the attack name belongs to
    atkname         - the attack step name


    Return:
    The list of the parent attack steps related to the specified attack step.
    The return value is a list of tuples, where each tuple is in the format
    (class, atkstepname).
    """
    return get_internal_parents(lang, cls, atkname) + get_external_parents(lang, cls, atkname)



def get_variables_for_class(lang_spec: dict, object_class: str) -> dict:
    """
    Get all Variables for a specific Class
    NOTE: Variables are the ones specified in MAL through `let` statements

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      mal language as provided by `read_language_spec(...)`
    object_class    - a string representing the class for which we want to list
                      the variables

    Return:
    A dictionary representing the set variables for the specified class.
    """
    variables = {}

    def _get_variables(object_class):
        nonlocal variables
        for asset in lang_spec["assets"]:
            if asset["name"] == object_class:
                var_stmts = asset["variables"]
                for var in var_stmts:
                    if not var["name"] in variables:
                        variables[var["name"]] = var
                if asset["superAsset"]:
                    _get_variables(asset["superAsset"])

    _get_variables(object_class)
    return variables


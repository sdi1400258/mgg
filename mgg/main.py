# -*- encoding: utf-8 -*-
# mgg v0.1.0
# Generate MAL-based attack graphs
# Copyright 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.

"""
mgg main Module

:Copyright: 2021, Giuseppe Nebbione.
:License: GPLv3 (see /LICENSE).
"""

__all__ = ()

import sys
import json
from mgg.cl_parser import parse_args

import mgg.language as l
import mgg.model as m
import mgg.securicad as s
import mgg.ingestor.networkx as mggnx
import mgg.ingestor.rldefender as mggrld
import mgg.ingestor.neo4j as mggneo
from mgg import atkgraph


def main():
    """Main routine of mgg."""
    args = parse_args(sys.argv[1:])
    cmd_params = vars(args)

    if cmd_params['command'] == 'scad':
        model_scad = s.load_model_from_scad_archive(cmd_params['archive'])
        m.save_model_to_json(model_scad, "tmp/intermediate.json")
        lang_scad = s.load_language_specification(cmd_params['language'])
        graph = atkgraph.generate_graph(lang_scad, model_scad)
        graph = atkgraph.attach_attacker(model_scad, graph)

        mggneo.ingest_model(model_scad, delete=True)
        mggneo.ingest(graph, delete=False)
        atkgraph.save_atkgraph(graph,"tmp/atkgraph.json")
    elif cmd_params['command'] == 'tests':

        # Test Scenario: 1 Computer 2 App
        model_test = s.load_model_from_scad_archive("tests/assets/testLang1/models/testlang1_1computer_2app.sCAD")
        lang_test = s.load_language_specification("tests/assets/testLang1/org.mal-lang.testlang1-1.0.0.mar")
        m.save_model_to_json(model_test,"tmp/1comp_1app.json")
        graph = atkgraph.generate_graph(lang_test, model_test)
        atkgraph.save_atkgraph(graph,"tmp/testlang1.json")

        # Test loading of complex securicad archive
        model_scad = s.load_model_from_scad_archive("tests/assets/coreLang/models/petest4.sCAD")
        lang_scad = s.load_language_specification("tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar")

        # Test testlang2
        model_scad = s.load_model_from_scad_archive("tests/assets/testLang2/models/2A_1B_1C_2D.sCAD")
        lang_scad = s.load_language_specification("tests/assets/testLang2/org.mal-lang.testlang2-1.0.0.mar")
        graph = atkgraph.generate_graph(lang_scad, model_scad)
        atkgraph.save_atkgraph(graph,"tmp/scad_archive_output.json")

        # attached_nodes = ["AA:2245388199686721192:a1",
        #                   "AA:2245388199686721192:a2",
        #                   "AA:-214940586777624565:a1"]
        # graph = atkgraph.attach_attacker_and_compute(lang_scad,graph,attached_nodes)

        # Test testlang3
        model_scad = s.load_model_from_scad_archive("tests/assets/testLang3/models/1_cable_2_traffic.sCAD")
        lang_scad = s.load_language_specification("tests/assets/testLang3/org.mal-lang.testlang3-1.0.0.mar")

        graph = atkgraph.generate_graph(lang_scad, model_scad)
        atkgraph.save_atkgraph(graph,"tmp/scad_archive_output.json")

        # Test testlang4
        model_scad = s.load_model_from_scad_archive("tests/assets/testLang4/models/directories_branches-2-3.sCAD")
        lang_scad = s.load_language_specification("tests/assets/testLang4/org.mal-lang.testlang4-1.0.0.mar")
        graph = atkgraph.generate_graph(lang_scad, model_scad)

        # Test CoreLang defenses
        model_scad = s.load_model_from_scad_archive("tests/assets/coreLang/models/atk_enabled_defenses.sCAD")
        lang_scad = s.load_language_specification("tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar")
        graph = atkgraph.generate_graph(lang_scad, model_scad)
        graph = atkgraph.attach_attacker(model_scad, graph)

        mggneo.ingest(graph, delete=True)
        atkgraph.save_atkgraph(graph,"tmp/graph_with_defense_status_and_attacker.json")
    elif cmd_params['command'] == 'intermediate':
        with open(cmd_params['json'], 'r') as fp:
            model = json.load(fp)
        lang_scad = s.load_language_specification(cmd_params['language'])
        with open("tmp/lang.json", 'w') as fp:
            json.dump(lang_scad, fp, indent = 2)
        graph = atkgraph.generate_graph(lang_scad, model)
        graph = atkgraph.attach_attacker(model, graph)

        atkgraph.save_atkgraph(graph, "tmp/atkgraph.json")

        mggneo.ingest_model(model, delete=True)
        mggneo.ingest(graph, delete=False)

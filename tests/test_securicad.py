#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# mgg test suite
# Copyright © 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.


import json
import zipfile
import xml.etree.ElementTree as ET

import pytest
import mgg
import mgg.model as m
import mgg.language as l
import mgg.securicad as s





def test_get_language_specification():
    lang = s.load_language_specification("tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar")
    assert "assets" in lang.keys()
    assert "associations" in lang.keys()




scad_archive = "/home/gnc/mgg/tests/assets/simple.sCAD"


